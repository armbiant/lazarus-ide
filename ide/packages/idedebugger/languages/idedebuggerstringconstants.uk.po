msgid ""
msgstr ""
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-SourceCharset: utf-8\n"

#: idedebuggerstringconstants.dbgbreakgroupdlgcaption
msgctxt "lazarusidestrconsts.dbgbreakgroupdlgcaption"
msgid "Select Groups"
msgstr "Виберіть групи"

#: idedebuggerstringconstants.dbgbreakgroupdlgheaderdisable
msgid "Select groups to disable when breakpoint is hit"
msgstr "Виберіть групи для вимкнення при задіянні точки зупинки"

#: idedebuggerstringconstants.dbgbreakgroupdlgheaderenable
msgid "Select groups to enable when breakpoint is hit"
msgstr "Виберіть групи для ввімкнення при задіянні точки зупинки"

#: idedebuggerstringconstants.dbgbreakpropertygroupnotfound
#, object-pascal-format
msgid "Some groups in the Enable/Disable list do not exist.%0:sCreate them?%0:s%0:s%1:s"
msgstr "Деякі групи в списку ввімкнення/вимкнення не існують.%0:sСтворити їх?%0:s%0:s%1:s"

#: idedebuggerstringconstants.dbgdispformatbinary
#, fuzzy
msgctxt "idedebuggerstringconstants.dbgdispformatbinary"
msgid "Binary"
msgstr "Двійковий"

#: idedebuggerstringconstants.dbgdispformatcharacter
#, fuzzy
msgctxt "idedebuggerstringconstants.dbgdispformatcharacter"
msgid "Character"
msgstr "Символ"

#: idedebuggerstringconstants.dbgdispformatdecimal
#, fuzzy
msgctxt "idedebuggerstringconstants.dbgdispformatdecimal"
msgid "Decimal"
msgstr "Десяткове"

#: idedebuggerstringconstants.dbgdispformatdefault
#, fuzzy
msgctxt "idedebuggerstringconstants.dbgdispformatdefault"
msgid "Default"
msgstr "Типово"

#: idedebuggerstringconstants.dbgdispformatfloatingpoin
#, fuzzy
msgctxt "idedebuggerstringconstants.dbgdispformatfloatingpoin"
msgid "Floating Point"
msgstr "Рухома кома"

#: idedebuggerstringconstants.dbgdispformathexadecimal
#, fuzzy
msgctxt "idedebuggerstringconstants.dbgdispformathexadecimal"
msgid "Hexadecimal"
msgstr "Шістнадцяткове"

#: idedebuggerstringconstants.dbgdispformatmemorydump
#, fuzzy
msgctxt "idedebuggerstringconstants.dbgdispformatmemorydump"
msgid "Memory Dump"
msgstr "Дамп пам'яті"

#: idedebuggerstringconstants.dbgdispformatpointer
#, fuzzy
msgctxt "idedebuggerstringconstants.dbgdispformatpointer"
msgid "Pointer"
msgstr "Вказівник"

#: idedebuggerstringconstants.dbgdispformatrecordstruct
#, fuzzy
msgctxt "idedebuggerstringconstants.dbgdispformatrecordstruct"
msgid "Record/Structure"
msgstr "Запис/Структура"

#: idedebuggerstringconstants.dbgdispformatstring
#, fuzzy
msgctxt "idedebuggerstringconstants.dbgdispformatstring"
msgid "String"
msgstr "Рядок"

#: idedebuggerstringconstants.dbgdispformatunsigned
#, fuzzy
msgctxt "idedebuggerstringconstants.dbgdispformatunsigned"
msgid "Unsigned"
msgstr "Непідписаний"

#: idedebuggerstringconstants.dlgbackconvoptaction
#, fuzzy
msgctxt "idedebuggerstringconstants.dlgbackconvoptaction"
msgid "Action"
msgstr "Дія"

#: idedebuggerstringconstants.dlgbackconvoptaddnew
msgid "Add"
msgstr ""

#: idedebuggerstringconstants.dlgbackconvoptdebugoptions
msgid "Backend Converter"
msgstr ""

#: idedebuggerstringconstants.dlgbackconvoptmatchtypesbyname
msgid "Match types by name"
msgstr ""

#: idedebuggerstringconstants.dlgbackconvoptname
#, fuzzy
msgctxt "idedebuggerstringconstants.dlgbackconvoptname"
msgid "Name"
msgstr "Назва"

#: idedebuggerstringconstants.dlgbackconvoptremove
msgid "Remove"
msgstr ""

#: idedebuggerstringconstants.dlgbackendconvoptdebugconverter
msgid "Backend Converter:"
msgstr ""

#: idedebuggerstringconstants.dlgbackendconvoptdefault
msgid "- Default -"
msgstr ""

#: idedebuggerstringconstants.dlgbackendconvoptdisabled
msgid "- Disabled -"
msgstr ""

#: idedebuggerstringconstants.dlgenvtype
msgctxt "lazarusidestrconsts.dlgenvtype"
msgid "Type"
msgstr "Тип"

#: idedebuggerstringconstants.dlgfilterall
msgctxt "lazarusidestrconsts.dlgfilterall"
msgid "All files"
msgstr "Всі файли"

#: idedebuggerstringconstants.dlgfilterxml
msgctxt "lazarusidestrconsts.dlgfilterxml"
msgid "XML files"
msgstr "Файли XML"

#: idedebuggerstringconstants.dlgidedbgdebugger
msgid "Debugger"
msgstr ""

#: idedebuggerstringconstants.dlgidedbgentername
msgid "Enter name"
msgstr ""

#: idedebuggerstringconstants.dlgidedbgnewitem
msgid "New Item"
msgstr ""

#: idedebuggerstringconstants.dlginspectamountofitemstoshow
msgid "Amount of items to show"
msgstr ""

#: idedebuggerstringconstants.dlginspectboundsdd
#, object-pascal-format
msgid "Bounds: %d .. %d"
msgstr ""

#: idedebuggerstringconstants.dlginspectindexoffirstitemtoshow
msgid "Index of first item to show"
msgstr ""

#: idedebuggerstringconstants.dlgunitdeprefresh
msgctxt "lazarusidestrconsts.dlgunitdeprefresh"
msgid "Refresh"
msgstr "Оновити"

#: idedebuggerstringconstants.dlgvaluecolor
msgctxt "lazarusidestrconsts.dlgvaluecolor"
msgid "Value"
msgstr "Значення"

#: idedebuggerstringconstants.dlgvaluedataaddr
msgid "Data-Address"
msgstr ""

#: idedebuggerstringconstants.drsaddwatch
#, fuzzy
msgctxt "idedebuggerstringconstants.drsaddwatch"
msgid "Add watch"
msgstr "Додати спостереження"

#: idedebuggerstringconstants.drsappendresultatbottomofhis
msgid "Append result at bottom of history"
msgstr ""

#: idedebuggerstringconstants.drsbreakpointcolwidthaction
msgid "Action column"
msgstr "Графа дії"

#: idedebuggerstringconstants.drsbreakpointcolwidthcondition
msgid "Condition column"
msgstr "Графа умови"

#: idedebuggerstringconstants.drsbreakpointcolwidthfile
msgid "File/address column"
msgstr "Графа файлу/адреси"

#: idedebuggerstringconstants.drsbreakpointcolwidthgroup
msgid "Group column"
msgstr "Графа групи"

#: idedebuggerstringconstants.drsbreakpointcolwidthline
msgctxt "debuggerstrconst.drsbreakpointcolwidthline"
msgid "Line column"
msgstr "Графа рядка"

#: idedebuggerstringconstants.drsbreakpointcolwidthpasscount
msgid "Pass-count column"
msgstr "Графа числа проходів"

#: idedebuggerstringconstants.drscolwidthbrkpointimg
msgid "Break indication column"
msgstr "Графа індикації зупинки"

#: idedebuggerstringconstants.drscolwidthexpression
msgid "Expression column"
msgstr "Графа виразу"

#: idedebuggerstringconstants.drscolwidthfunc
msgid "Function name column"
msgstr "Графа назви функції"

#: idedebuggerstringconstants.drscolwidthindex
msgid "Index column"
msgstr "Графа індексу"

#: idedebuggerstringconstants.drscolwidthline
msgctxt "debuggerstrconst.drscolwidthline"
msgid "Line column"
msgstr "Графа рядка"

#: idedebuggerstringconstants.drscolwidthname
msgid "Name column"
msgstr "Графа назви"

#: idedebuggerstringconstants.drscolwidthsource
msgid "Source column"
msgstr "Графа джерела"

#: idedebuggerstringconstants.drscolwidthstate
msgid "State column"
msgstr "Графа стану"

#: idedebuggerstringconstants.drscolwidthvalue
msgid "Value column"
msgstr "Графа значення"

#: idedebuggerstringconstants.drsdebugconverter
msgid "Converter"
msgstr ""

#: idedebuggerstringconstants.drsdisableenableupdatesforth
#, fuzzy
msgctxt "idedebuggerstringconstants.drsdisableenableupdatesforth"
msgid "Disable/Enable updates for the entire window"
msgstr "Вимкнення/Ввімкнення оновлень всього вікна"

#: idedebuggerstringconstants.drsenterexpression
msgid "Enter Expression"
msgstr ""

#: idedebuggerstringconstants.drsevaluate
msgid "Evaluate"
msgstr ""

#: idedebuggerstringconstants.drshistory
#, fuzzy
msgctxt "idedebuggerstringconstants.drshistory"
msgid "History"
msgstr "Історія"

#: idedebuggerstringconstants.drshistorycolwidthcurrent
msgid "Current column"
msgstr "Поточна графа"

#: idedebuggerstringconstants.drshistorycolwidthlocation
msgid "Location column"
msgstr "Графа розміщення"

#: idedebuggerstringconstants.drshistorycolwidthtime
msgid "Time column"
msgstr "Графа часу"

#: idedebuggerstringconstants.drsinsertresultattopofhistor
msgid "Insert result at top of history"
msgstr ""

#: idedebuggerstringconstants.drsinspect
msgid "Inspect"
msgstr ""

#: idedebuggerstringconstants.drsinspectcolwidthdataclass
msgid "Data class column"
msgstr "Графа класу даних"

#: idedebuggerstringconstants.drsinspectcolwidthdataname
msgid "Data name column"
msgstr "Графа назви даних"

#: idedebuggerstringconstants.drsinspectcolwidthdatatype
msgid "Data type column"
msgstr "Графа типу даних"

#: idedebuggerstringconstants.drsinspectcolwidthdatavalue
msgid "Data value column"
msgstr "Графа значення даних"

#: idedebuggerstringconstants.drsinspectcolwidthdatavisibility
msgid "Data visibility column"
msgstr "Графа видимості даних"

#: idedebuggerstringconstants.drsinspectcolwidthmethaddress
msgid "Method address column"
msgstr "Графа адреси методу"

#: idedebuggerstringconstants.drsinspectcolwidthmethname
msgid "Method name column"
msgstr "Графа назви методу"

#: idedebuggerstringconstants.drsinspectcolwidthmethreturns
msgid "Method returns column"
msgstr "Графа значення методу"

#: idedebuggerstringconstants.drsinspectcolwidthmethtype
msgid "Method type column"
msgstr "Графа типу методу"

#: idedebuggerstringconstants.drslen
#, object-pascal-format
msgid "Len=%d: "
msgstr "Дов=%d: "

#: idedebuggerstringconstants.drsnewvalue
msgid "New Value"
msgstr ""

#: idedebuggerstringconstants.drsnewvaluetoassigntothevari
msgid "New value to assign to the variable in the debugged process (use Shift-Enter to confirm)"
msgstr ""

#: idedebuggerstringconstants.drsnodebugconverter
msgid "No Converter"
msgstr ""

#: idedebuggerstringconstants.drsnohistorykept
msgid "No history kept"
msgstr ""

#: idedebuggerstringconstants.drsrunallthreadswhileevaluat
msgid "Run all threads while evaluating"
msgstr ""

#: idedebuggerstringconstants.drssuspend
msgid "Suspend"
msgstr ""

#: idedebuggerstringconstants.drsusefunctioncalls
#, fuzzy
msgctxt "idedebuggerstringconstants.drsusefunctioncalls"
msgid "Function"
msgstr "Функція"

#: idedebuggerstringconstants.drsusefunctioncallshint
msgid "Allow function calls"
msgstr ""

#: idedebuggerstringconstants.drsuseinstanceclass
#, fuzzy
msgctxt "idedebuggerstringconstants.drsuseinstanceclass"
msgid "Instance"
msgstr "Примірник"

#: idedebuggerstringconstants.drsuseinstanceclasshint
#, fuzzy
msgctxt "idedebuggerstringconstants.drsuseinstanceclasshint"
msgid "Use Instance class type"
msgstr "Викор. тип екземпляра класу"

#: idedebuggerstringconstants.drsuseinstanceclasstype
msgctxt "idedebuggerstringconstants.drsuseinstanceclasstype"
msgid "Use Instance class type"
msgstr "Викор. тип екземпляра класу"

#: idedebuggerstringconstants.drswatchsplitterinspect
msgctxt "idedebuggerstringconstants.drswatchsplitterinspect"
msgid "Inspect pane"
msgstr "Панель огляду"

#: idedebuggerstringconstants.dsrevalusedebugconverter
msgid "Use Backend Converter"
msgstr ""

#: idedebuggerstringconstants.histdlgbtnclearhint
msgid "Clear all snapshots"
msgstr "Очистити всі знімки"

#: idedebuggerstringconstants.histdlgbtnenablehint
msgid "Toggle view snapshot or current"
msgstr "Перемкнути перегляд між знімком та поточним"

#: idedebuggerstringconstants.histdlgbtnmakesnaphint
msgid "Take Snapshot"
msgstr "Зробити знімок"

#: idedebuggerstringconstants.histdlgbtnpowerhint
msgid "Switch on/off automatic snapshots"
msgstr "Ввімкнути/Вимкнути автоматичні знімки"

#: idedebuggerstringconstants.histdlgbtnremovehint
msgid "Remove selected entry"
msgstr "Видалити вибраний елемент"

#: idedebuggerstringconstants.histdlgbtnshowhisthint
msgid "View history"
msgstr "Переглянути історію"

#: idedebuggerstringconstants.histdlgbtnshowsnaphint
msgid "View Snapshots"
msgstr "Переглянути знімки"

#: idedebuggerstringconstants.histdlgcolumnloc
msgctxt "lazarusidestrconsts.histdlgcolumnloc"
msgid "Location"
msgstr "Розміщення"

#: idedebuggerstringconstants.histdlgcolumntime
msgid "Time"
msgstr "Час"

#: idedebuggerstringconstants.histdlgformname
msgctxt "lazarusidestrconsts.histdlgformname"
msgid "History"
msgstr "Історія"

#: idedebuggerstringconstants.lisactions
msgid "Actions:"
msgstr "Дії:"

#: idedebuggerstringconstants.lisaddress
msgid "Address:"
msgstr "Адреса:"

#: idedebuggerstringconstants.lisaddressbreakpoint
msgctxt "lazarusidestrconsts.lisaddressbreakpoint"
msgid "&Address Breakpoint ..."
msgstr "&Точка зупинки за адресою ..."

#: idedebuggerstringconstants.lisallowfunctio
msgid "Allow Function Calls"
msgstr "Дозволити виклики функцій"

#: idedebuggerstringconstants.lisautocontinueafter
msgid "Auto continue after:"
msgstr "Автопродовження після:"

#: idedebuggerstringconstants.lisbpsdisabled
msgctxt "lazarusidestrconsts.lisbpsdisabled"
msgid "Disabled"
msgstr "Вимкнено"

#: idedebuggerstringconstants.lisbpsenabled
msgctxt "lazarusidestrconsts.lisbpsenabled"
msgid "Enabled"
msgstr "Увімкнено"

#: idedebuggerstringconstants.lisbreak
msgctxt "lazarusidestrconsts.lisbreak"
msgid "Break"
msgstr "Перервати"

#: idedebuggerstringconstants.lisbreakpointproperties
msgid "Breakpoint Properties"
msgstr "Властивості точки зупинки"

#: idedebuggerstringconstants.lisbrkpointaction
msgctxt "idedebuggerstringconstants.lisbrkpointaction"
msgid "Action"
msgstr "Дія"

#: idedebuggerstringconstants.lisbrkpointstate
msgctxt "idedebuggerstringconstants.lisbrkpointstate"
msgid "State"
msgstr "Стан"

#: idedebuggerstringconstants.liscallstacknotevaluated
msgid "Stack not evaluated"
msgstr "Стек не обчислений"

#: idedebuggerstringconstants.lisccoerrorcaption
msgctxt "lazarusidestrconsts.lisccoerrorcaption"
msgid "Error"
msgstr "Помилка"

#: idedebuggerstringconstants.lisclear
msgid "Clear"
msgstr ""

#: idedebuggerstringconstants.liscoladdress
msgid "Address"
msgstr "Адреса"

#: idedebuggerstringconstants.liscolclass
msgid "Class"
msgstr "Клас"

#: idedebuggerstringconstants.liscolreturns
msgid "Returns"
msgstr "Повертає"

#: idedebuggerstringconstants.liscolvisibility
msgid "Visibility"
msgstr "Видимість"

#: idedebuggerstringconstants.liscondition
msgid "Condition"
msgstr "Умова"

#: idedebuggerstringconstants.liscopyall
msgid "Copy All"
msgstr "Копіювати все"

#: idedebuggerstringconstants.liscopyalloutputclipboard
msgid "Copy all output to clipboard"
msgstr "Копіювати весь вивід у буфер"

#: idedebuggerstringconstants.liscsbottom
msgctxt "lazarusidestrconsts.liscsbottom"
msgid "Bottom"
msgstr "Нижній"

#: idedebuggerstringconstants.liscstop
msgctxt "lazarusidestrconsts.liscstop"
msgid "Top"
msgstr "Верхній"

#: idedebuggerstringconstants.liscurrent
msgctxt "lazarusidestrconsts.liscurrent"
msgid "Current"
msgstr "Поточний"

#: idedebuggerstringconstants.lisdadattach
msgid "Attach"
msgstr "Приєднати"

#: idedebuggerstringconstants.lisdadimagename
msgid "Image Name"
msgstr "Назва зображення"

#: idedebuggerstringconstants.lisdadpid
msgid "PID"
msgstr "Ідентифікатор процесу"

#: idedebuggerstringconstants.lisdadrunningprocesses
msgid "Running Processes"
msgstr "Запущені процеси"

#: idedebuggerstringconstants.lisdbgallitemdelete
msgctxt "lazarusidestrconsts.lisdbgallitemdelete"
msgid "Delete all"
msgstr "Видалити все"

#: idedebuggerstringconstants.lisdbgallitemdeletehint
msgctxt "lazarusidestrconsts.lisdbgallitemdeletehint"
msgid "Delete all"
msgstr "Видалити все"

#: idedebuggerstringconstants.lisdbgallitemdisable
msgctxt "lazarusidestrconsts.lisdbgallitemdisable"
msgid "Disable all"
msgstr "Вимкнути все"

#: idedebuggerstringconstants.lisdbgallitemdisablehint
msgctxt "lazarusidestrconsts.lisdbgallitemdisablehint"
msgid "Disable all"
msgstr "Вимкнути все"

#: idedebuggerstringconstants.lisdbgallitemenable
msgctxt "lazarusidestrconsts.lisdbgallitemenable"
msgid "Enable all"
msgstr "Ввімкнути все"

#: idedebuggerstringconstants.lisdbgallitemenablehint
msgctxt "lazarusidestrconsts.lisdbgallitemenablehint"
msgid "Enable all"
msgstr "Ввімкнути все"

#: idedebuggerstringconstants.lisdbgbreakpointpropertieshint
msgctxt "lazarusidestrconsts.lisdbgbreakpointpropertieshint"
msgid "Breakpoint Properties ..."
msgstr "Властивості точки зупинки ..."

#: idedebuggerstringconstants.lisdbgitemdeletehint
msgctxt "lazarusidestrconsts.lisdbgitemdeletehint"
msgid "Delete"
msgstr "Вилучити"

#: idedebuggerstringconstants.lisdbgitemdisable
msgctxt "lazarusidestrconsts.lisdbgitemdisable"
msgid "Disable"
msgstr "Вимкнути"

#: idedebuggerstringconstants.lisdbgitemdisablehint
msgctxt "lazarusidestrconsts.lisdbgitemdisablehint"
msgid "Disable"
msgstr "Вимкнути"

#: idedebuggerstringconstants.lisdbgitemenable
msgctxt "lazarusidestrconsts.lisdbgitemenable"
msgid "Enable"
msgstr "Ввімкнути"

#: idedebuggerstringconstants.lisdbgitemenablehint
msgctxt "lazarusidestrconsts.lisdbgitemenablehint"
msgid "Enable"
msgstr "Ввімкнути"

#: idedebuggerstringconstants.lisdbgterminal
msgctxt "lazarusidestrconsts.lisdbgterminal"
msgid "Console In/Output"
msgstr "Консольний ввід/вивід"

#: idedebuggerstringconstants.lisdbgwinpower
msgid "On/Off"
msgstr "Увімк/Вимк"

#: idedebuggerstringconstants.lisdbgwinpowerhint
msgctxt "idedebuggerstringconstants.lisdbgwinpowerhint"
msgid "Disable/Enable updates for the entire window"
msgstr "Вимкнення/Ввімкнення оновлень всього вікна"

#: idedebuggerstringconstants.lisdebuggerfeedbackerror
msgid "Debugger Error"
msgstr "Помилка зневаджувача"

#: idedebuggerstringconstants.lisdebuggerfeedbackinformation
msgid "Debugger Information"
msgstr "Інформація зневаджувача"

#: idedebuggerstringconstants.lisdebuggerfeedbackwarning
msgid "Debugger Warning"
msgstr "Попередження зневаджувача"

#: idedebuggerstringconstants.lisdebugoptionsfrmresume
msgid "Resume"
msgstr "Відновити"

#: idedebuggerstringconstants.lisdebugoptionsfrmresumehandled
msgid "Resume Handled"
msgstr "Відновити оброблені"

#: idedebuggerstringconstants.lisdebugoptionsfrmresumeunhandled
msgid "Resume Unhandled"
msgstr "Відновити необроблені"

#: idedebuggerstringconstants.lisdeleteall
msgid "&Delete All"
msgstr "&Видалити всі"

#: idedebuggerstringconstants.lisdeleteallbreakpoints
msgid "Delete all breakpoints?"
msgstr "Видалити всі точки зупинки?"

#: idedebuggerstringconstants.lisdeleteallbreakpoints2
#, object-pascal-format
msgid "Delete all breakpoints in file \"%s\"?"
msgstr "Видалити всі точки зупинки з файлу \"%s\"?"

#: idedebuggerstringconstants.lisdeleteallinsamesource
msgid "Delete All in same source"
msgstr "Видалити всі в тому ж файлі сирцевого коду"

#: idedebuggerstringconstants.lisdeleteallselectedbreakpoints
msgid "Delete all selected breakpoints?"
msgstr "Видалити всі вибрані точки зупинки?"

#: idedebuggerstringconstants.lisdeletebreakpoint
msgid "Delete Breakpoint"
msgstr "Видалити точку зупинки"

#: idedebuggerstringconstants.lisdeletebreakpointatline
#, object-pascal-format
msgid "Delete breakpoint at%s\"%s\" line %d?"
msgstr "Видалити точку зупинки в%s\"%s\" рядок %d?"

#: idedebuggerstringconstants.lisdeletebreakpointforaddress
#, object-pascal-format
msgid "Delete breakpoint for address %s?"
msgstr "Видалити точку зупинки для адреси %s?"

#: idedebuggerstringconstants.lisdeletebreakpointforwatch
#, object-pascal-format
msgid "Delete watchpoint for \"%s\"?"
msgstr "Видалити точку спостереження для \"%s\"?"

#: idedebuggerstringconstants.lisdigits
msgid "Digits:"
msgstr "Розряди:"

#: idedebuggerstringconstants.lisdisableallinsamesource
msgid "Disable All in same source"
msgstr "Вимкнути всі у тому ж сирцевому файлі"

#: idedebuggerstringconstants.lisdisablebreakpoint
msgid "Disable Breakpoint"
msgstr "Вимкнути точку зупинки"

#: idedebuggerstringconstants.lisdisablegroups
msgid "Disable Groups"
msgstr "Вимкнути групи"

#: idedebuggerstringconstants.lisenableall
msgid "&Enable All"
msgstr "&Увімкнути все"

#: idedebuggerstringconstants.lisenableallinsamesource
msgid "Enable All in same source"
msgstr "Ввімкнути Всі в тому ж джерелі"

#: idedebuggerstringconstants.lisenablebreakpoint
msgid "Enable Breakpoint"
msgstr "Увімкнути точку зупинки"

#: idedebuggerstringconstants.lisenablegroups
msgid "Enable Groups"
msgstr "Ввімкнути групи"

#: idedebuggerstringconstants.lisevalexpression
msgctxt "lazarusidestrconsts.lisevalexpression"
msgid "Eval expression"
msgstr "Оцінити вираз"

#: idedebuggerstringconstants.lisevaluateall
msgid "Evaluate all"
msgstr "Обчислити всі"

#: idedebuggerstringconstants.lisevaluatemodify
msgid "&Evaluate/Modify"
msgstr "&Обчислити/Змінити"

#: idedebuggerstringconstants.liseventlogclear
msgid "Clear Events"
msgstr "Очистити події"

#: idedebuggerstringconstants.liseventlogoptions
msgid "Event Log Options ..."
msgstr "Параметри журналу подій ..."

#: idedebuggerstringconstants.liseventlogsavetofile
msgid "Save Events to File"
msgstr "Зберегти події у файл"

#: idedebuggerstringconstants.liseventslogaddcomment
msgid "Add Comment ..."
msgstr "Додати коментар ..."

#: idedebuggerstringconstants.liseventslogaddcomment2
msgid "Add Comment"
msgstr "Додати коментар"

#: idedebuggerstringconstants.lisexceptiondialog
msgid "Debugger Exception Notification"
msgstr "Повідомлення про виняток зневаджувача"

#: idedebuggerstringconstants.lisexport
msgctxt "lazarusidestrconsts.lisexport"
msgid "Export"
msgstr "Експортувати"

#: idedebuggerstringconstants.lisexpression
msgid "Expression:"
msgstr "Вираз:"

#: idedebuggerstringconstants.lisfilenameaddress
msgid "Filename/Address"
msgstr "Назва файлу/адреса"

#: idedebuggerstringconstants.lisfunction
msgctxt "lazarusidestrconsts.lisfunction"
msgid "Function"
msgstr "Функція"

#: idedebuggerstringconstants.lisgotoselected
msgid "Goto selected"
msgstr "Перейти до вибраного"

#: idedebuggerstringconstants.lisgroup
msgid "Group"
msgstr "Група"

#: idedebuggerstringconstants.lisgroupassignexisting
#, object-pascal-format
msgid "Assign to existing \"%s\" group?"
msgstr "Присвоїти до наявної \"%s\" групи?"

#: idedebuggerstringconstants.lisgroupemptydelete
#, object-pascal-format
msgid "No more breakpoints are assigned to group \"%s\", delete it?"
msgstr "До групи \"%s\" більше не призначено точок зупинки, видалити її?"

#: idedebuggerstringconstants.lisgroupemptydeletemore
#, object-pascal-format
msgid "%sThere are %d more empty groups, delete all?"
msgstr "%sЄ ще %d порожніх груп, видалити всі?"

#: idedebuggerstringconstants.lisgroupnameemptyclearinstead
msgid "The group name cannot be empty. Clear breakpoints' group(s)?"
msgstr "Назва групи не може бути порожня. Очистити груп(и) точок зупинки?"

#: idedebuggerstringconstants.lisgroupnameinput
msgid "Group name:"
msgstr "Назва групи:"

#: idedebuggerstringconstants.lisgroupnameinvalid
msgid "BreakpointGroup name must be a valid Pascal identifier name."
msgstr "Назва ГрупиТочокЗупинки має бути дійсною назвою ідентифікатора."

#: idedebuggerstringconstants.lisgroupsetnew
msgid "Set new group ..."
msgstr "Задати нову групу ..."

#: idedebuggerstringconstants.lisgroupsetnone
msgid "Clear group(s)"
msgstr "Очистити груп(и)"

#: idedebuggerstringconstants.lishitcount
msgid "Hitcount"
msgstr "К-сть влучень"

#: idedebuggerstringconstants.lisid
msgctxt "lazarusidestrconsts.lisid"
msgid "ID"
msgstr "ID"

#: idedebuggerstringconstants.lisignoreexceptiontype
msgid "Ignore this exception type"
msgstr "Нехтувати цей тип винятків"

#: idedebuggerstringconstants.lisimport
msgctxt "lazarusidestrconsts.lisimport"
msgid "Import"
msgstr "Імпортувати"

#: idedebuggerstringconstants.lisindex
msgid "Index"
msgstr "Індекс"

#: idedebuggerstringconstants.lisinspect
msgid "&Inspect"
msgstr "&Перевірити"

#: idedebuggerstringconstants.lisinspectaddwatch
msgctxt "lazarusidestrconsts.lisinspectaddwatch"
msgid "Add watch"
msgstr "Додати спостереження"

#: idedebuggerstringconstants.lisinspectclassinherit
#, object-pascal-format
msgid "%s: class %s inherits from %s"
msgstr "%s : клас %s успадковує від %s"

#: idedebuggerstringconstants.lisinspectdata
msgid "Data"
msgstr "Дані"

#: idedebuggerstringconstants.lisinspectdialog
msgid "Debug Inspector"
msgstr "Інспектор зневадження"

#: idedebuggerstringconstants.lisinspectmethods
msgid "Methods"
msgstr "Методи"

#: idedebuggerstringconstants.lisinspectpointerto
#, object-pascal-format
msgid "Pointer to %s"
msgstr "Вказівник на %s"

#: idedebuggerstringconstants.lisinspectproperties
msgctxt "lazarusidestrconsts.lisinspectproperties"
msgid "Properties"
msgstr "Властивості"

#: idedebuggerstringconstants.lisinspectshowcolclass
msgid "Show class column"
msgstr ""

#: idedebuggerstringconstants.lisinspectshowcoltype
msgid "Show type column"
msgstr ""

#: idedebuggerstringconstants.lisinspectshowcolvisibility
msgid "Show visibility column"
msgstr ""

#: idedebuggerstringconstants.lisinspectunavailableerror
#, object-pascal-format
msgid "%s: unavailable (error: %s)"
msgstr "%s: недоступно (помилка: %s)"

#: idedebuggerstringconstants.lisinspectuseinstance
msgctxt "idedebuggerstringconstants.lisinspectuseinstance"
msgid "Instance"
msgstr "Примірник"

#: idedebuggerstringconstants.lisinspectuseinstancehint
msgid "Use instance class"
msgstr "Використати клас примірника"

#: idedebuggerstringconstants.lisinvalidoff
msgid "Invalid (Off)"
msgstr "Неправильний (Вимк)"

#: idedebuggerstringconstants.lisinvalidon
msgid "Invalid (On)"
msgstr "Неправильний (Увімк)"

#: idedebuggerstringconstants.liskmevaluatemodify
msgid "Evaluate/Modify"
msgstr "Обчислити/Змінити"

#: idedebuggerstringconstants.lisless
msgctxt "lazarusidestrconsts.lisless"
msgid "Less"
msgstr "Менше"

#: idedebuggerstringconstants.lisline
msgid "Line:"
msgstr "Рядок:"

#: idedebuggerstringconstants.lislinelength
msgid "Line/Length"
msgstr "Рядок/Довжина"

#: idedebuggerstringconstants.lislocals
msgctxt "lazarusidestrconsts.lislocals"
msgid "Local Variables"
msgstr "Локальні змінні"

#: idedebuggerstringconstants.lislocalsdlgcopyall
msgid "Copy &all"
msgstr "Копіювати &всі"

#: idedebuggerstringconstants.lislocalsdlgcopyname
msgid "&Copy Name"
msgstr "&Копіювати назву"

#: idedebuggerstringconstants.lislocalsdlgcopynamevalue
msgid "Co&py Name and Value"
msgstr "Ко&піювати назву і значення"

#: idedebuggerstringconstants.lislocalsdlgcopyrawvalue
msgid "Copy &RAW Value"
msgstr "Копіювати &сирі значення"

#: idedebuggerstringconstants.lislocalsdlgcopyvalue
msgid "C&opy Value"
msgstr "Ко&піювати значення"

#: idedebuggerstringconstants.lislocalsnotevaluated
msgid "Locals not evaluated"
msgstr "Локальні не обчислені"

#: idedebuggerstringconstants.lislogcallstack
msgid "Log Call Stack"
msgstr "Стек викликів журналу"

#: idedebuggerstringconstants.lislogcallstacklimit
msgid "(frames limit. 0 - no limits)"
msgstr "(межа фреймів. 0 - необмежено)"

#: idedebuggerstringconstants.lislogevalexpression
msgctxt "lazarusidestrconsts.lislogevalexpression"
msgid "Eval expression"
msgstr "Оцінити вираз"

#: idedebuggerstringconstants.lislogmessage
msgid "Log Message"
msgstr "Повідомлення журналу"

#: idedebuggerstringconstants.lismaxs
#, object-pascal-format
msgid "Max %d"
msgstr "Макс %d"

#: idedebuggerstringconstants.lismenubreak
msgid "&Break"
msgstr "&Перервати"

#: idedebuggerstringconstants.lismenuhelp
msgid "&Help"
msgstr "&Довідка"

#: idedebuggerstringconstants.lismenuviewbreakpoints
msgid "BreakPoints"
msgstr "Точки зупину"

#: idedebuggerstringconstants.lismenuviewcallstack
msgid "Call Stack"
msgstr "Стек викликів"

#: idedebuggerstringconstants.lismenuviewdebugevents
msgctxt "lazarusidestrconsts.lismenuviewdebugevents"
msgid "Event Log"
msgstr "Журнал подій"

#: idedebuggerstringconstants.lismenuviewdebugoutput
msgid "Debug Output"
msgstr "Повідомлення зневаджувача"

#: idedebuggerstringconstants.lismore
msgctxt "lazarusidestrconsts.lismore"
msgid "More"
msgstr "Більше"

#: idedebuggerstringconstants.lisms
msgid "(ms)"
msgstr "(мс)"

#: idedebuggerstringconstants.lismvsavemessagestofiletxt
msgid "Save messages to file (*.txt)"
msgstr "Зберегти повідомлення у файл (*.txt)"

#: idedebuggerstringconstants.lisname
msgctxt "lazarusidestrconsts.lisname"
msgid "Name"
msgstr "Назва"

#: idedebuggerstringconstants.lisoff
msgid "? (Off)"
msgstr "? (Викл)"

#: idedebuggerstringconstants.lison
msgid "? (On)"
msgstr "? (Вкл)"

#: idedebuggerstringconstants.lispasscount
msgid "Pass Count"
msgstr "Кількість Проходів"

#: idedebuggerstringconstants.lispefilename
msgid "Filename:"
msgstr "Назва файлу"

#: idedebuggerstringconstants.lispendingon
msgid "Pending (On)"
msgstr "Очікування (Увімкн.)"

#: idedebuggerstringconstants.lisregisters
msgctxt "lazarusidestrconsts.lisregisters"
msgid "Registers"
msgstr "Регістри"

#: idedebuggerstringconstants.lisrepeatcount
msgid "Repeat Count:"
msgstr "Повторень:"

#: idedebuggerstringconstants.lissourcebreakpoint
msgid "&Source Breakpoint ..."
msgstr "Точка зупинки &джерела ..."

#: idedebuggerstringconstants.lissrcline
#, fuzzy
msgctxt "idedebuggerstringconstants.lissrcline"
msgid "Line"
msgstr "Рядок"

#: idedebuggerstringconstants.lisstop
msgctxt "lazarusidestrconsts.lisstop"
msgid "Stop"
msgstr "Закінчити"

#: idedebuggerstringconstants.lisstyle
msgctxt "lazarusidestrconsts.lisstyle"
msgid "Style"
msgstr "Стиль"

#: idedebuggerstringconstants.listakesnapshot
msgid "Take a Snapshot"
msgstr "Зробити знімок"

#: idedebuggerstringconstants.listhreads
msgctxt "lazarusidestrconsts.listhreads"
msgid "Threads"
msgstr "Нитки"

#: idedebuggerstringconstants.listhreadscurrent
msgctxt "lazarusidestrconsts.listhreadscurrent"
msgid "Current"
msgstr "Поточний"

#: idedebuggerstringconstants.listhreadsfunc
msgctxt "lazarusidestrconsts.listhreadsfunc"
msgid "Function"
msgstr "Функція"

#: idedebuggerstringconstants.listhreadsgoto
msgid "Goto"
msgstr "ЙтиДо"

#: idedebuggerstringconstants.listhreadsline
msgctxt "lazarusidestrconsts.listhreadsline"
msgid "Line"
msgstr "Рядок"

#: idedebuggerstringconstants.listhreadsnotevaluated
msgid "Threads not evaluated"
msgstr "Нитки не визначені"

#: idedebuggerstringconstants.listhreadssrc
msgctxt "lazarusidestrconsts.listhreadssrc"
msgid "Source"
msgstr "Джерело"

#: idedebuggerstringconstants.listhreadsstate
msgctxt "lazarusidestrconsts.listhreadsstate"
msgid "State"
msgstr "Стан"

#: idedebuggerstringconstants.lisvalue
#, fuzzy
msgctxt "idedebuggerstringconstants.lisvalue"
msgid "Value"
msgstr "Значення"

#: idedebuggerstringconstants.lisviewbreakpointproperties
msgctxt "lazarusidestrconsts.lisviewbreakpointproperties"
msgid "Breakpoint Properties ..."
msgstr "Властивості точки зупинки ..."

#: idedebuggerstringconstants.lisviewsource
msgid "View Source"
msgstr "Дивитись джерело"

#: idedebuggerstringconstants.lisviewsourcedisass
msgctxt "lazarusidestrconsts.lisviewsourcedisass"
msgid "View Assembler"
msgstr "Перемкнути вигляд Асемблер"

#: idedebuggerstringconstants.liswatch
msgid "&Watch"
msgstr "&Спостерігати"

#: idedebuggerstringconstants.liswatchdata
msgid "Watch:"
msgstr "Огляд:"

#: idedebuggerstringconstants.liswatchkind
msgid "Watch action"
msgstr "Дія огляду"

#: idedebuggerstringconstants.liswatchkindread
msgid "Read"
msgstr "Читання"

#: idedebuggerstringconstants.liswatchkindreadwrite
msgid "Read/Write"
msgstr "Читання/Запис"

#: idedebuggerstringconstants.liswatchkindwrite
msgid "Write"
msgstr "Запис"

#: idedebuggerstringconstants.liswatchpoint
msgid "&Data/Watch Breakpoint ..."
msgstr "Точка зупинки &даних/огляду ..."

#: idedebuggerstringconstants.liswatchpointbreakpoint
msgid "&Data/watch Breakpoint ..."
msgstr "Точка зупинки &даних/огляду ..."

#: idedebuggerstringconstants.liswatchpropert
msgid "Watch Properties"
msgstr "Властивості Спостерігання"

#: idedebuggerstringconstants.liswatchscope
msgid "Watch scope"
msgstr "Межі огляду"

#: idedebuggerstringconstants.liswatchscopeglobal
msgctxt "lazarusidestrconsts.liswatchscopeglobal"
msgid "Global"
msgstr "Глобально"

#: idedebuggerstringconstants.liswatchscopelocal
msgid "Declaration"
msgstr "Оголошення"

#: idedebuggerstringconstants.liswatchtowatchpoint
msgid "Create &Data/Watch Breakpoint ..."
msgstr "Створити точку зупинки &даних/огляду ..."

#: idedebuggerstringconstants.liswldeleteall
msgid "De&lete All"
msgstr "Ви&далити всі"

#: idedebuggerstringconstants.liswldisableall
msgid "D&isable All"
msgstr "Н&едоступні Всі"

#: idedebuggerstringconstants.liswlenableall
msgid "E&nable All"
msgstr "В&вімкнути все"

#: idedebuggerstringconstants.liswlexpression
msgid "Expression"
msgstr "Вираз"

#: idedebuggerstringconstants.liswlinspectpane
msgctxt "idedebuggerstringconstants.liswlinspectpane"
msgid "Inspect pane"
msgstr "Панель перегляду"

#: idedebuggerstringconstants.liswlproperties
msgid "&Properties"
msgstr "&Властивості"

#: idedebuggerstringconstants.liswlwatchlist
msgctxt "lazarusidestrconsts.liswlwatchlist"
msgid "Watches"
msgstr "Спостереження"

#: idedebuggerstringconstants.regdlgbinary
msgctxt "lazarusidestrconsts.regdlgbinary"
msgid "Binary"
msgstr "Двійковий"

#: idedebuggerstringconstants.regdlgdecimal
msgctxt "lazarusidestrconsts.regdlgdecimal"
msgid "Decimal"
msgstr "Десятковий"

#: idedebuggerstringconstants.regdlgdefault
msgctxt "idedebuggerstringconstants.regdlgdefault"
msgid "Default"
msgstr ""

#: idedebuggerstringconstants.regdlgdisplaytypeforselectedregisters
msgid "Display type for selected Registers"
msgstr "Показувати тип вибраних регістрів"

#: idedebuggerstringconstants.regdlgformat
msgid "Format"
msgstr "Формат"

#: idedebuggerstringconstants.regdlghex
msgid "Hex"
msgstr "Шістнадцятковий"

#: idedebuggerstringconstants.regdlgoctal
msgid "Octal"
msgstr "Вісімковий"

#: idedebuggerstringconstants.regdlgraw
msgid "Raw"
msgstr "Сирі дані"

#: idedebuggerstringconstants.rsattachto
msgid "Attach to"
msgstr "Приєднати до"

#: idedebuggerstringconstants.rsenterpid
msgid "Enter PID"
msgstr "Введіть ідентифікатор процесу"

#: idedebuggerstringconstants.synfnewvalueisempty
msgid "\"New value\" is empty."
msgstr "\"Нове значення\" порожнє."

#: idedebuggerstringconstants.synfthedebuggerwasnotabletomodifythevalue
msgid "The debugger was not able to modify the value."
msgstr "Зневаджувач не зміг змінити значення."

#: idedebuggerstringconstants.uemtogglebreakpoint
msgid "Toggle &Breakpoint"
msgstr "Перемкнути &точку зупинки"

